// ** User List Component
import Table from './Table'

// ** Styles
import 'src/@core/scss/react/apps/app-users.scss'

const UsersList = () => {
  return (
    <div className='app-user-list'>
      <Table />
    </div>
  )
}

export default UsersList
