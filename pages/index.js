import VerticalLayout from "src/@core/layouts/VerticalLayout"

const Home = () => {
  return (
    <VerticalLayout>
      <h1>Hello Index</h1>
    </VerticalLayout>
  )
}

export default Home