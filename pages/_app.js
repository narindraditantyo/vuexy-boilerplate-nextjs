// ** Fake Database
import "src/@fake-db";

// ** PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'
import 'prismjs/components/prism-jsx.min'

// ** React Perfect Scrollbar
import 'react-perfect-scrollbar/dist/css/styles.css'

// ** Third Party Packages
import ResizeObserver from "resize-observer-polyfill";

// ** React Toastify
import 'styles/@core/react/libs/toastify/toastify.scss'

// ** Core Styling
import "styles/index.scss";
import 'src/@core/assets/fonts/feather/iconfont.css'

const App = ({ Component, pageProps }) => {
  // Prevent error on macOS and iOS devices
  if (typeof window !== "undefined") {
    if (!window.ResizeObserver) {
      window.ResizeObserver = ResizeObserver;
    }
  }

  const getLayout = Component.getLayout || ((page) => page)
  return getLayout(<Component {...pageProps} />);
};

export default App;
